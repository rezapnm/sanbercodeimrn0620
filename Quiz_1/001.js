//Jawaban A
console.log("Jawaban A")
function balikString (string = "")
{
    let balik = ''
    for(let a = string.length-1; a>=0 ; a--)
    {
        balik += string[a]
    }
    return balik
}
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"))

//Jawaban B
console.log("====================================\nJawaban B")
function palindrome (string)
{
    let balik = ''
    for(let a = string.length-1 ; a>=0 ; a--)
    {
        balik += string[a]
    }
    return (string.toLowerCase() == balik.toLowerCase())
}
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))

//Jawaban C
console.log("====================================\nJawaban C")
function bandingkan (num1 = 0, num2 = 0)
{
    if (num1<0 || num2<0 || (num1==num2))
    {
        return -1
    }
    if (num1 > num2)
    {
        return num1
    }
    if (num2 > num1)
    {
        return num2
    }
}

console.log(bandingkan(10, 15))
console.log(bandingkan(12, 12))
console.log(bandingkan(-1, 10)) 
console.log(bandingkan(112, 121))
console.log(bandingkan(1))
console.log(bandingkan())
console.log(bandingkan("15", "18"))