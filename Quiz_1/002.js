//Jawaban A
console.log("Jawaban A")
function AscendingTen (num)
{
    if (!num)
    {
        return -1
    }
    else
    {
        var result = ""
        for(let i = num; i < num+10; i++)
        {
            result += i + " "
        }
        return result
    }
}
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())

//Jawaban B
console.log("===================================\nJawaban B")
function DescendingTen (num)
{
    if (!num)
    {
        return -1
    }
    else
    {
        var result = ""
        for(let i = num; i > num-10; i--)
        {
            result += i + " "
        }
        return result
    }
}
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())

//Jawaban C
console.log("===================================\nJawaban C")
function ConditionalAscDesc(reference, check)
{
    if (!check){ return -1}
    else
    {
        let result = ""
        if(check%2 == 1)
        {
            for(let i = reference; i < reference+10; i++)
            {
                result += i + " "
            }
            return result
        }
        else 
        {
            for(let i = reference; i > reference-10; i--)
            {
                result += i + " "
            }
            return result
        }
    }    
}
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc())

//Jawaban D
console.log("===================================\nJawaban D")
function ularTangga()
{
    let result = ""
    for(let row = 10; row >= 1; row--)
    {
        if(row%2 == 0)
        {
            for(let column = row*10; column > (row-1)*10; column--)
            {
                result += column + " "   
            }
            result += "\n"
        }
        if(row%2 == 1)
        {
            for(let column = ((row-1)*10)+1; column<= row*10; column++)
            {
                result += column + " "
            }
            result += "\n"
        }
    }
    return result
}
console.log(ularTangga())