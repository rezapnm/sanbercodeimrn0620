class Score {
    constructor(subject, points, email){
        this._subject = subject
        this._points = points
        this._email = email
    }
    average(){
        let sum = 0
        for(let i = 0; i < this._points.length; i++){
            sum += this._points[i]
        }
        return (sum/this._points.length).toFixed(1)
    }
}

let viewScores = (data = Array, subject) => {
    let result = []
    for(let i = 1; i < data.length; i++){
        let obj = {}
        obj.email = data[i][0]
        obj.subject = subject
        if(subject.split(" ").join("") == "quiz-1"){obj.points = data[i][1]}
        if(subject.split(" ").join("") == "quiz-2"){obj.points = data[i][2]}
        if(subject.split(" ").join("") == "quiz-3"){obj.points = data[i][3]}
        result.push(obj)
    }
    console.log(result)
}

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
console.log("\n")

let recapScore = (data = Array)=>{
    for(let i = 1; i < data.length; i++){
        let email, rata, predikat
        email = data[i][0]
        rata = new Score("",data[i].slice(1,4),"").average()
        if (rata > 90){predikat = "honour"}
        else if (rata > 80){predikat = "graduate"}
        else if (rata > 70){predikat = "participate"}
        console.log(`${i}. Email : ${email}\nRata-rata : ${rata}\nPredikat : ${predikat}\n`)
    }
}
recapScore(data)