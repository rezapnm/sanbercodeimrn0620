var string = ""
var string1 = "I love coding"
var string2 = "I will become a mobile developer"
var santai = "santai"
var kualitas = "berkualitas"
var love = "I Love Coding"
var loop = 0

//======================================================================================
//Jawaban 1
console.log("==============================\nNOMOR 1")
console.log("LOOPING PERTAMA")
while (loop < 20 )
{
    loop+=2
    console.log(loop + " - " + string1)
}
console.log("LOOPING KEDUA")
while (loop >= 2 )
{
    console.log(loop+ " - " + string2)
    loop-=2
}

//Jawaban 2
console.log("==============================\nNOMOR 2")
for (var a = 0; a < 20; a++)
{
    var angka = a+1
    if(angka%2 == 1) // jika angka ganjil
    {
        if(angka%3 == 0) // jika angka kelipatan 3
        {
            console.log(angka + " - " + love)
        }
        else
        {
            console.log(angka + " - " + santai)
        }
    }
    else if(angka%2 == 0) // jika angka genap
    {
        console.log(angka + " - " + kualitas)
    }
} 

//Jawaban 3
console.log("==============================\nNOMOR 3")
for(var row = 1 ; row <= 4 ;  row ++ )
{
    for(var column = 1 ; column <=8 ; column ++)
    {
        string+= "#"
    }
    string+="\n"
}
console.log(string)

//Jawaban 4
console.log("==============================\nNOMOR 4")
string = ""
for(var row = 1; row <= 7 ; row++)
{
    for(var column = 1; column <= row; column++)
    {
        string+= "#"
    }
    string+="\n"
}
console.log(string)

//Jawaban 5
console.log("==============================\nNOMOR 5")
string = ""
for(var row = 1; row <= 8; row++)
{
    for(var column = 1; column <=4; column++)
    {
        if(row%2 == 1)
        {
            string+=" #"
        }
        else
        {
            string+="# "
        }
    }
    string+= "\n"
}
console.log(string)