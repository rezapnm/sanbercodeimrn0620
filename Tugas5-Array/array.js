// Jawaban 1
function range(startNum, finishNum)
{
    var arr = []
    if (!startNum || !finishNum)
    {
        return -1
    }
    if (startNum > finishNum)
    {
        for(let a = startNum; a >= finishNum; a--)
        {
            arr.push(a)
        }
        return arr
    }
    if (startNum < finishNum)
    {
        for(let a = startNum; a <= finishNum; a++)
        {
            arr.push(a)
        }
        return arr
    }
}
console.log("Jawaban 1")
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

/*============================================================
Jawaban 2*/
console.log("======================================\nJawaban 2")
function rangeWithStep(startNum, finishNum, step)
{
    let arr = []
    if (!startNum || !finishNum)
    {
        return -1
    }
    if (startNum > finishNum)
    {
        for(let i = startNum; i >= finishNum; i -= step)
        {
            arr.push(i)
        }
        return arr
    }
    if (startNum < finishNum)
    {
        for(let i = startNum; i <= finishNum; i += step)
        {
            arr.push(i)
        }
        return arr
    }
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

/*============================================================
Jawaban 3*/
console.log("======================================\nJawaban 3")
function sum(firstNum = 0 , lastNum = 0, step = 1)
{
    let total = 0
    if(firstNum > lastNum)
    {
        for(let a = firstNum; a >= lastNum; a -= step)
        {
            total += a
        }
    }
    if(firstNum < lastNum)
    {
        for(let a = firstNum; a <= lastNum; a += step)
        {
            total += a
        }
    }
    return total
}
console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum()) 
/*============================================================
Jawaban 4*/
console.log("======================================\nJawaban 4")
function dataHandling (database)
{
    let identity = ""
    for(let row = 0; row < database.length; row++)
    {
        identity += (
        "Nomor ID\t:" + database[row][0] +
        "\nNama Lengkap\t:" + database[row][1]+
        "\nTTL\t\t:" + database[row][2]+ " " +database[row][3]+
        "\nHobi\t\t:" + database[row][4]+
        "\n\n"
        )
    }
    return identity
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
] 
console.log(dataHandling(input))

/*============================================================
Jawaban 5*/
console.log("======================================\nJawaban 5")
function balikKata(string)
{
    var kata = ""
    for(var i = string.length-1; i >= 0; i--)
    {
        kata += string [i]
    }
    return kata
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

/*============================================================
Jawaban 6*/
console.log("======================================\nJawaban 6")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(data =[])
{
    var lastName = "Elsharawy"
    var fullName =  data.slice(1,2) + lastName
    var provinsi = "Provinsi " + data.slice(2,3)
    var kelamin  = "Pria"
    var sekolah  = "SMA Internasional Metro"
    var tanggal  = String(data[3]).split("/")
    var bulan = Number(tanggal[1])

    switch(bulan)
    {
        case 1 : {bulan = 'Januari'; break;}
        case 2 : {bulan = 'Februari'; break;}
        case 3 : {bulan = 'Maret'; break;}
        case 4 : {bulan = 'April'; break;}
        case 5: {bulan = 'Mei'; break;}
        case 6 : {bulan = 'Juni'; break;}
        case 7 : {bulan = 'Juli'; break;}
        case 8 : {bulan = 'Agustus'; break;}
        case 9 : {bulan = 'September'; break;}
        case 10 : {bulan = 'Oktober'; break;}
        case 11 : {bulan = 'November'; break;}
        case 12 : {bulan = 'Desember'; break;}
        default : bulan = 'Tidak valid'
    }
    data.splice(1,1,fullName)
    data.splice(2,1,provinsi)
    data.splice(4,1,kelamin,sekolah)
    var descTanggal = tanggal.slice().sort((a,b) => (b-a)) 
    var nama = String(data.slice(1,2)).slice(0,14)   
    console.log(data)
    console.log(bulan)
    console.log(descTanggal)
    console.log(tanggal.join("-"))
    console.log(nama)
    
}
dataHandling2(input)