//jawaban 1
console.log("==========================\nJawaban 1")
function arrayToObject(arr = Array)
{
    var error = []
    var now = new Date()
    var thisYear = now.getFullYear()
    if (!arr){console.log(arr)}
    else
    {
        for (let i = 0; i < arr.length ; i++)
        {
            var obj = new Object()
            for(let a = 0; a < arr[i].length; a++)
            {
                obj.firstName = arr[i][0]
                obj.lastName = arr[i][1]
                obj.gender = arr[i][2]
                if (arr[i][3] < thisYear)
                {
                    obj.age = thisYear - arr[i][3]
                }
                else {obj.age = "Invalid Birth Year"}
                
            }
            console.log(i+1 + '.', obj.firstName + " " + obj.lastName , obj)
        }
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people)
arrayToObject(people2)
arrayToObject([])
var empty = []
console.log(empty)

//jawaban 2
console.log("==================================\nJawaban 2")
var barang = {
    SepatuStacattu : {brand : "Sepatu Stacattu", price : 1500000},
    BajuZorro : {brand : "Baju Zorro", price : 500000},
    BajuHN : {brand : "Baju H&N", price : 250000},
    SweaterUniklooh : {brand : "Sweater UnikLooh", price : 175000},
    CasingHandphone : {brand : "Casing Handphone", price : 50000}
}
function shoppingTime(memberId, money)
{
    var obj = new Object()
    var cash = money
    var list = []
    if(!memberId)
    {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if(money < 50000)
    {
        return "Mohon maaf, uang tidak cukup"
    }
    else
    {
        while(cash >= 50000)
        {
            if(cash >= 1500000)
            {
                list.push(barang.SepatuStacattu.brand)
                cash -= barang.SepatuStacattu.price
            }
            else if(cash >= 500000)
            {
                list.push(barang.BajuZorro.brand)
                cash -= barang.BajuZorro.price 
            }
            else if(cash >= 250000)
            {
                list.push(barang.BajuHN.brand)
                cash -= barang.BajuHN.price
            }
            else if (cash >= 175000)
            {
                list.push(barang.SweaterUniklooh.brand)
                cash -= barang.SweaterUniklooh.price
            }
            else if (cash >= 50000)
            {
                list.push(barang.CasingHandphone.brand)
                cash -= barang.CasingHandphone.price
                break
            }
        }
        obj.memberId = memberId
        obj.money = money
        obj.listPurchased = list
        obj.changeMoney = cash
        return (obj)
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

//jawaban 3
console.log("==================================\nJawaban 3")
function naikAngkot(listPenumpang = Array)
{
    var list = []
    for (var i = 0; i < listPenumpang.length; i++)
    {
        var obj = {}
            obj.penumpang = listPenumpang[i][0]
            obj.naikDari = listPenumpang[i][1]
            obj.tujuan = listPenumpang[i][2]
        var bayar = 0
        var awal = listPenumpang[i][1].charCodeAt()
        var akhir = listPenumpang[i][2].charCodeAt()
        for(var a = awal; a < akhir; a++)
        {
            bayar += 2000
        }
        obj.bayar = bayar
        list.push(obj)
    }
    return list
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))