//Jawaban 1
console.log("==============\nJawaban1")
const golden = () => {
    console.log("this is golden!")
}
golden()
//Jawaban 2
console.log("==============\nJawaban2")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () =>{
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("William", "Imoh").fullName()
//Jawaban 3
console.log("==============\nJawaban3")
const newObject = {
    firstName : "Harry",
    lastName : "Potter Holt",
    destination : "Hogwarts React Conf",
    occupation : "Deve-wizard Avocado",
    spell : "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation}= newObject
console.log(firstName, lastName, destination, occupation)
//Jawaban 4
console.log("==============\nJawaban4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)
//Jawaban 5
console.log("==============\nJawaban5")
const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet, `
    before += `consectetur adipiscing elit, ${planet} do eiusmod tempor `
    before += `incididunt ut labore et dolore magna aliqua. Ut enim `
    before += `ad minim veniam`
console.log(before)