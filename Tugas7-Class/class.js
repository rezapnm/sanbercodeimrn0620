//Jawaban 1
console.log("==========================\nJawaban 1")
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this._name = name
        this._legs = legs
        this._cold_blooded = cold_blooded
    }
    get name() {
        return this._name
    }
    get legs() {
        return this._legs
    }
    get cold_blooded(){
        return this._cold_blooded
    }
}
class Frog extends Animal {
    constructor(name, legs, cold_blooded){
        super(name,legs,cold_blooded)
    }
    jump(){
        console.log("hop hop")
    }
}
class Ape extends Animal {
    constructor(name,legs = 2,cold_blooded){
        super(name,cold_blooded)
        this._legs = legs
    }
    yell(){
        console.log("Auooo")
    }
}

var sheep = new Animal("shaun")
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump()

var sungokong = new Ape ("kera sakti")
sungokong.yell()

//Jawaban 2
console.log("==========================\nJawaban 2")
class Clock {
    constructor({template}){
        this._template = template
    }
    render(){
        var date = new Date()

        var hours = date.getHours()
        if(hours < 10) hours = '0' + hours

        var mins = date.getMinutes()
        if(mins < 10) mins = '0' + mins

        var secs = date.getSeconds()
        if(secs < 10) secs = '0' + secs

        var output = this._template
            .replace('h',hours)
            .replace('m',mins)
            .replace('s',secs)
        
        console.log(output)
    }
    stop(){
        clearInterval(this._timer)
    }
    start(){
        this.render()
        this._timer = setInterval(this.render.bind(this),1000)
    }
}
var clock = new Clock({template : 'h:m:s'})
clock.start()