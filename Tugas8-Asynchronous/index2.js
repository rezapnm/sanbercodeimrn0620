var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var start = function start(index,times){
    if(index<books.length){
        readBooksPromise(times,books[index])
            .then(function(berhasil){
                return start(index+1, times-books[index].timeSpent)
            })
            .catch(function(error){
                console.log(error)
            })
    }
}
start(0,10000)