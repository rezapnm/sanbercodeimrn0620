var readBooks = require('./callback.js')

var books = [
    {name : 'LOTR', timeSpent: 3000},
    {name : 'Fidas', timeSpent: 2000},
    {name : 'Kalkulus', timeSpent: 4000}
]

var start = function start(index,times){
    if(index<books.length){
        readBooks(times,books[index],function(timeLeft){
            return start (index +1, timeLeft)
        })
    }
}

start(0,10000)

